from setuptools import setup, find_packages

setup(
    name="ai3test",
    version="0.1",
    description="Checks for the ai3 infrastructure.",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    install_requires=["PyYAML", "nose", "jinja2"],
    zip_safe=False,
    packages=find_packages(),
)
