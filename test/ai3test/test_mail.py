import email
import imaplib
import logging
import os
import re
import smtplib
import time
from email.utils import formatdate
from email.mime.text import MIMEText

from float_integration_test import ANSIBLE_VARS
from ai3test import *


class TestMail(TestBase):

    users = {
        'encrypted': {
            'name': AI3_TEST_PARAMS['encrypted_user']['name'],
            'password': AI3_TEST_PARAMS['encrypted_user']['password'],
        },
        'nonpriv': {
            'name': AI3_TEST_PARAMS['nonpriv_user']['name'],
            'password': AI3_TEST_PARAMS['nonpriv_user']['password'],
        },
    }

    def setUp(self):
        self.skip_unless_role_exists('mail-backend')
        TestBase.setUp(self)

    def test_imap_connect_ok(self):
        imap = imaplib.IMAP4_SSL(self.frontend_ip())
        imap.login(
            self.users['nonpriv']['name'],
            self.users['nonpriv']['password'])
        imap.select('INBOX')
        imap.close()

    def test_imap_connect_fail_without_ssl(self):
        # Fails when running on Docker as we are local.
        self.skipTest('temporarily disabled')
        imap = imaplib.IMAP4(self.frontend_ip())
        self.assertRaises(
            Exception,
            imap.login,
            self.users['nonpriv']['name'],
            self.users['nonpriv']['password'])

    def test_imap_connect_fail_with_bad_credentials(self):
        imap = imaplib.IMAP4(self.frontend_ip())
        self.assertRaises(
            Exception,
            imap.login, 'foo@example.com', 'wrong password')

    def _create_msg(self, sender, rcpt):
        unique_id = os.urandom(4).hex()
        msg = MIMEText('This is a test message, please ignore.')
        msg['Subject'] = 'test message #%s' % unique_id
        msg['X-UniqueID'] = unique_id
        msg['From'] = sender
        msg['To'] = rcpt
        msg['Date'] = formatdate()
        return msg, unique_id

    def _send_email(self, sender, sender_password, rcpt, from_addr=None):
        if not from_addr:
            from_addr = sender
        msg, unique_id = self._create_msg(from_addr, rcpt)
        logging.info('created new message <%s> from %s', unique_id, from_addr)

        conn = smtplib.SMTP(self.frontend_ip(), 587)
        conn.ehlo()
        conn.starttls()
        conn.ehlo()
        conn.login(sender, sender_password)
        conn.sendmail(
            from_addr, [rcpt], msg.as_string())
        conn.quit()
        logging.info('sent message <%s> to %s', unique_id, rcpt)

        return unique_id

    def _wait_for_message(self, imap_user, imap_password, match_header, match_value,
                          timeout=300):
        imap = imaplib.IMAP4_SSL(self.frontend_ip())
        imap.login(imap_user, imap_password)
        imap.select('INBOX')

        def _check_for_msg():
            logging.info('looking for message %s', match_value)
            typ, data = imap.uid(
                'search', None, 'HEADER', match_header, match_value)
            if typ == 'OK' and data[0]:
                logging.info('message %s found, fetching uuid "%s"',
                             match_value, data[0])
                typ, msgdata = imap.uid('fetch', data[0], '(RFC822)')
                if typ != 'OK':
                    raise Exception('error fetching message: %s' % typ)
                msg = msgdata[0][1]
                imap.uid('store', data[0], '+FLAGS', r'(\Deleted)')
                imap.expunge()
                logging.info('message %s deleted successfully', match_value)
                return msg
            return None

        deadline = time.time() + timeout
        while time.time() < deadline:
            msg = _check_for_msg()
            if msg:
                return True
            time.sleep(1)
        return False

    # Send an email from a to b and find it in c's IMAP account.
    def _send_email_and_read_it(self, sender, sender_password, rcpt,
                                imap_user, imap_password):
        unique_id = self._send_email(sender, sender_password, rcpt)

        delivered = self._wait_for_message(imap_user, imap_password,
                                           'X-UniqueID', unique_id)

        self.assertTrue(delivered)
        return msg

    def test_send_email_and_read_it(self):
        self._send_email_and_read_it(
            self.users['nonpriv']['name'], self.users['nonpriv']['password'],
            self.users['nonpriv']['name'],
            self.users['nonpriv']['name'], self.users['nonpriv']['password'])

    def test_send_email_and_read_it_encrypted_mailbox(self):
        self._send_email_and_read_it(
            self.users['encrypted']['name'], self.users['encrypted']['password'],
            self.users['encrypted']['name'],
            self.users['encrypted']['name'], self.users['encrypted']['password'])

    def test_send_email_to_alias_and_read_it(self):
        self._send_email_and_read_it(
            self.users['encrypted']['name'], self.users['encrypted']['password'],
            'due-alias1@investici.org',
            self.users['encrypted']['name'], self.users['encrypted']['password'])

    def test_send_email_is_properly_anonymized(self):
        # Fails when running on Docker as we are local.
        self.skipTest('temporarily disabled')
        msgbody = self._send_email_and_read_it(
            self.users['nonpriv']['name'], self.users['nonpriv']['password'],
            self.users['nonpriv']['name'],
            self.users['nonpriv']['name'], self.users['nonpriv']['password'])
        msg = email.message_from_string(msgbody)

        # Find the Received header that has the authenticated sender
        # information, and verify that it has been anonymized.
        auth_rcvd_hdr = None
        for received in msg.get_all('Received'):
            if 'Authenticated sender' in received:
                auth_rcvd_hdr = received
                break
        self.assertTrue(auth_rcvd_hdr,
                        'Could not find Received header with auth info')
        self.assertTrue(
            re.match(r'^from \[[0-9.]+\] \(mx1\.investici\.org ',
                     auth_rcvd_hdr),
            'Received header does not seem anonymized: %s' % auth_rcvd_hdr)

    def test_smtp_spf_fail(self):
        # example.com has a SPF record that should cause the delivery to fail.
        sender = 'external@example.com'
        rcpt = self.users['nonpriv']['name']

        msg, unique_id = self._create_msg(sender, rcpt)

        conn = smtplib.SMTP(self.frontend_ip(), 25)
        conn.ehlo()
        self.assertRaises(
            smtplib.SMTPRecipientsRefused,
            conn.sendmail,
            sender, [rcpt], msg.as_string())

    def _send_with_xclient(self, sender, sender_hostname, sender_addr, rcpt):
        # Send a message pretending to be a real client using the
        # XCLIENT SMTP extension.
        msg, unique_id = self._create_msg(sender, rcpt)

        conn = smtplib.SMTP(self.frontend_ip(), 25)
        #conn.debuglevel = 3
        conn.ehlo()
        if not conn.has_extn('xclient'):
            self.skipTest('no XCLIENT support')
        conn.putcmd('XCLIENT', 'NAME=%s ADDR=%s' % (sender_hostname, sender_addr))
        conn.getreply()

        conn.ehlo(sender_hostname)
        conn.sendmail(
            sender, [rcpt], msg.as_string())

    def test_smtp_spf_pass(self):
        # Test a successful delivery from a real Internet host.
        sender = 'ale@incal.net'
        sender_hostname = 'giap.incal.net'
        sender_addr = '78.46.196.100'
        rcpt = self.users['nonpriv']['name']
        self._send_with_xclient(sender, sender_hostname, sender_addr, rcpt)

    def test_smtp_bounce_nonexisting_users(self):
        # Verify that deliveries to unknown users are rejected
        # immediately and not queued.
        sender = 'ale@incal.net'
        sender_hostname = 'giap.incal.net'
        sender_addr = '78.46.196.100'
        rcpt = 'nonexisting@investici.org'
        self.assertRaises(
            smtplib.SMTPRecipientsRefused,
            self._send_with_xclient,
            sender, sender_hostname, sender_addr, rcpt)

    def test_send_email_as_alias(self):
        # Verify that we can send emails with From (and envelope
        # sender) set to an alias of the logged-in user.
        self._send_email(
            'due@investici.org',
            'password',
            'uno@investici.org',
            from_addr='due-alias1@investici.org')

    def test_send_email_with_spoofed_from(self):
        # Verify that we can't send an email with an arbitrary
        # (spoofed) From address.
        self.assertRaises(
            smtplib.SMTPRecipientsRefused,
            self._send_email,
            'due@investici.org',
            'password',
            'uno@investici.org',
            from_addr='uno@investici.org')

    def test_mailman_send_message(self):
        # The test user sends an email to a list it is subscribed to.
        self._send_email_and_read_it(
            self.users['nonpriv']['name'],
            self.users['nonpriv']['password'],
            'lista@investici.org',
            self.users['nonpriv']['name'],
            self.users['nonpriv']['password'])
