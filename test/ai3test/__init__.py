from float_integration_test import *
import yaml
import os


_ai3_params_file = os.getenv(
    'AI3_TEST_PARAMS', os.path.join(os.path.dirname(__file__), 'ai3-test-params.yml'))
with open(_ai3_params_file) as fd:
    AI3_TEST_PARAMS = yaml.safe_load(fd)


class TestBase(TestBase):

    def skip_unless_role_exists(self, role):
        found = any(x == role for x in ANSIBLE_VARS['services'].keys())
        if not found:
            self.skipTest(f'role {role} not active')
