from ai3test import *


class TestPannello(TestBase):

    def setUp(self):
        # The SSO service, which we configure with keystore
        # integration, will fail unless the mail-backend service
        # exists.
        self.skip_unless_role_exists('mail-backend')
        TestBase.setUp(self)

    def test_login(self):
        c = self.sso_conversation(
            sso_username=AI3_TEST_PARAMS['nonpriv_user']['name'],
            sso_password=AI3_TEST_PARAMS['nonpriv_user']['password'],
        )
        url = 'https://accounts.' + ANSIBLE_VARS['domain_public'][0] + '/'
        result = c.request(url, self.frontend_ip())
        self.assertFalse(result.get('error'))
        self.assertEqual(200, result['status'])
        # The following check works both if we are looking at the
        # pannello homepage, and if we are sent directly to a forced
        # page (such as set-password-recovery-hint etc).
        # We simply match part of the common <title>.
        self.assertTrue(
            'Accounts' in str(result['body']),
            'Unexpected pannello homepage:\n%s' % result['body'])
