import urllib.request, urllib.error, urllib.parse

from ai3test import *
from float_integration_test.http import request


class WebUsersBase(TestBase):

    def setUp(self):
        self.skip_unless_role_exists('web-users')
        TestBase.setUp(self)

    def _assert_endpoint_ok(self, public_endpoint_name, path="", status=200,
            username=None, password=None):
        endpoint = public_endpoint_name
        url = 'https://%s.%s/%s' % (
            endpoint, ANSIBLE_VARS['domain_public'][0], path)
        handlers = []
        if username and password:
            mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            mgr.add_password(None, url, username, password)
            h = urllib.request.HTTPBasicAuthHandler(mgr)
            handlers.append(h)
        result = request(url, self.frontend_ip(), handlers=handlers)
        self.assertFalse(result.get('error'))
        self.assertEqual(status, result['status'],
                '%s replied %s' % (url, result['status']))

    def _assert_web_url_ok(self, path, *args, **kwargs):
       return self._assert_endpoint_ok('www', path, *args, **kwargs)


class WebDavTest(WebUsersBase):

    def test_webdav_ok(self):
        username = AI3_TEST_PARAMS['web_user']['name']
        password = AI3_TEST_PARAMS['web_user']['password']

        self._assert_web_url_ok('dav/%s' % username, username=username,
                password=password)


class SubsiteTest(WebUsersBase):

    def test_subsite_ok(self):
        username = AI3_TEST_PARAMS['web_user']['name']
        self._assert_web_url_ok(username + '/')
        self._assert_web_url_ok(username, status=301)

    def test_append_slash(self):
        username = AI3_TEST_PARAMS['web_user']['name']
        url = 'https://www.%s/%s' % (
            ANSIBLE_VARS['domain_public'][0], username)
        result = request(url, self.frontend_ip())
        self.assertEqual(result['location'], url + '/')
