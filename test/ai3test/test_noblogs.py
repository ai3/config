from ai3test import *
from float_integration_test.http import request


class NoblogsTest(TestBase):

    def setUp(self):
        self.skip_unless_role_exists('noblogs')
        TestBase.setUp(self)

    def test_noblogs_ok(self):
        result = request('https://noblogs.org/', self.frontend_ip())
        self.assertFalse(result.get('error'))
        self.assertEqual(200, result['status'])
        self.assertTrue(
            b'Information disorder was not enough' in result['body'],
            'Unexpected noblogs homepage:\n%s' % result['body'])

    def test_cavallette_ok(self):
        result = request('https://cavallette.noblogs.org/', self.frontend_ip())
        self.assertFalse(result.get('error'))
        self.assertEqual(200, result['status'])
        self.assertTrue(
            b'il blog di autistici.org' in result['body'],
            'Unexpected cavallette homepage:\n%s' % result['body'])
