This role configures the LDAP backend for the
[auth-server](https://git.autistici.org/id/auth).
