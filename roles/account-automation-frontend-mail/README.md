This role installs
[ai-scripts](https://git.autistici.org/ai/ai-scripts) cron jobs that
are associated with the *mail* service and run on the *frontend*
nodes.

In practice, these are configuration (*config-\**) cron jobs.
