#!/bin/sh

set -eu

test -d /var/lib/mailman/qfiles || exit 0

queues="archive retry commands virgin bounces shunt out in news"

for queue in $queues; do
  metric="mailman_qfiles_total{queue=\"${queue}\"}"
  value="-1"
  qdir="/var/lib/mailman/qfiles/${queue}"
  if [ -d $qdir ]; then
    value=$(find $qdir -type f | wc -l)
  fi
  echo "${metric} ${value}"
done
