Role to install *mailman*, a mailing list manager, as a partitioned
service. The containers depend on
[ai-scripts](https://git.autistici.org/ai/ai-scripts) to create and
configure the lists, as well as the integrations with other services
such as Postfix.

This role takes care of service configuration, setting up additional
facilities such as backups, Prometheus exporters, and creating the
various necessary data directories.

## Integration with NGINX

The ai-scripts *config-nginx* tool creates NGINX maps that route every
Mailman URL to the right backend, which is possible because the list
name is part of the URL itself
(www.autistici.org/mailman/*list-name*/...).

## Integration with Postfix

Incoming email gets delivered to Mailman via Postfix over LMTP, which
is a simpler version of SMTP that does not support queuing. This is
very important because we want to be able to bounce undesired emails
(*including* emails refused by Mailman itself: spam, unsubscribed
members to moderated lists, *etc*) as early as possible, to avoid
generating bounces from our own infrastructure.

For this reason, Postfix integration happens both in the *postfix-in*
instance (to bounce incoming messages at the perimeter), and in
*postfix-out* (to allow internal routing of messages from all other
sources).

## Integration with *list-archive*

The *list-archive* service is a critical dependency of the *mailman*
service (it serves the archives of public mailing lists), and mailman
delivers messages to it using the internal SMTP infrastructure.

To achieve this, Mailman will forward archived messages through
*mail-frontend:10025* (the internal SMTP relay), with a recipient of

> lurker+LISTNAME@list-archive.investici.org

For this to work, it is necessary to define a routing map that sends
*list-archive.investici.org* emails to the *list-archive* SMTP
endpoint, see [the list-archive role
documentation](../list-archive/README.md) for further details.
