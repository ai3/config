This role installs the
[ai-scripts](https://git.autistici.org/ai/ai-scripts) configuration
relevant to the scripts that need to run on *backend* nodes (meaning:
co-located with user data).

It installs a number of data files consumed by *ai-scripts*,
including:

* GPG keys for the automation account (robot@)
* credentials for the internal *rsync* service
* credentials for the external *cold-storage* service
