Customize the sso-server configuration (already provided by float),
and set up the associated
[auth-server](https://git.autistici.org/id/auth) service.

Installs our own auth-server config for sso-server that includes the
LDAP user backend.
