This role installs the
[ai-scripts](https://git.autistici.org/ai/ai-scripts) configuration
for those scripts that run on *frontend* nodes, and it provides the
"glue" for integrating those into float's own automation.

Integration highlights:

* the *templates/dns-base.yml.j2* file contains the `@hosted_base` and
  `@hosted_mail_mixin` DNS configuration templates, that define most
  records common to user-owned hosted DNS zones.

* installs the IMAP-autoconfig and MTA-STS configuration files that
  are common to all hosted domains (and are served by NGINX,
  referenced by the configuration files created by
  *ai-scripts/config-nginx*).
