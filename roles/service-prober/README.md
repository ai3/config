Runs
[ai3/tools/service-prober](https://git.autistici.org/ai3/tools/service-prober)
to run application-level tests against our services, and export the
results as Prometheus metrics.
