This service runs a parallel log ingestion stack along float's primary
one, but running with a separate TLS CA. It is meant to collect logs
from external ("aux") hosts, that are not under float's control.
