---

# The defaults file will stop slapd from ever starting, even on the
# first install.
- copy:
    content: "SLAPD_NO_START=1"
    dest: "/etc/default/slapd"

# We used to do this in the ldap slack role, but it seems they make
# things worse and cause slapd to attempt an initial installation.
#- name: Pre-seed the slapd package to skip configuration
#  shell: "echo slapd slapd/no_configuration boolean true | debconf-set-selections"
#- file: path=/etc/ldap/slapd.d state=directory
- name: Pre-seed the slapd package to skip configuration
  debconf:
    name: slapd
    question: slapd/no_configuration
    value: true
    vtype: boolean
  changed_when: false

# If we install slapd this way, it won't even create the openldap
# user, so we have to do it ourselves.
- name: Create the openldap group
  group:
    name: openldap
    system: yes
- name: Create the openldap user
  user:
    name: openldap
    group: openldap
    home: /var/lib/ldap
    createhome: no
    system: yes

# Install the defaults file for the exporter.
- name: Install slapd-prometheus-exporter config
  template:
    src: "slapd-prometheus-exporter.default.j2"
    dest: "/etc/default/slapd-prometheus-exporter"

# Make sure the monitoring exporter can access the LDAP socket.
- file:
    path: /etc/systemd/system/slapd-prometheus-exporter.service.d
    state: directory
- name: Customize slapd-prometheus-exporter config
  copy:
    dest: /etc/systemd/system/slapd-prometheus-exporter.service.d/float.conf
    content: |
      [Service]
      Group=openldap
  notify: reload slapd-prometheus-exporter

- name: Create /etc/ldap
  file:
    path: /etc/ldap
    state: directory

# Install the LDAP configuration
- name: Install LDAP configuration (dirs)
  file:
    path: "/etc/ldap/{{ item.path }}"
    state: directory
  with_filetree: config/
  when: item.state == 'directory'

- name: Install LDAP configuration (files)
  copy:
    src: "{{ item.src }}"
    dest: "/etc/ldap/{{ item.path }}"
  with_filetree: config/
  when: item.state == 'file'
  notify:
    - reload slapd

- name: Assign server ID
  set_fact:
    ldap_server_id: "{{item.0 + 1}}"
  with_indexed_items: "{{groups['ldap']}}"
  when: "item.1 == inventory_hostname"

- name: Install LDAP configuration (slapd.conf)
  template:
    src: "slapd.conf.j2"
    dest: "/etc/ldap/slapd.conf"
  notify:
    - reload slapd

# Now on to the database: create the database directory mountpoint(s).
- name: Create /var/lib/ldap
  file:
    path: /var/lib/ldap
    owner: openldap
    group: openldap
    mode: 0700
    state: directory

- name: Create /var/lib/ldap-accesslog
  file:
    path: /var/lib/ldap-accesslog
    owner: openldap
    group: openldap
    mode: 0700
    state: directory
  when: float_ldap_is_master

# Mount /var/lib/ldap as a tmpfs on slaves (setup fstab).
- name: Setup /var/lib/ldap tmpfs
  lineinfile:
    path: "/etc/fstab"
    line: "none /var/lib/ldap tmpfs rw,uid=openldap,gid=openldap,mode=0700,size={{ ldap_tmpfs_size }} 0 0"
  when: "not float_ldap_is_master"

# The opposite of the above: on the master, ensure /var/lib/ldap is
# *not* mounted as tmpfs, and actually unmount it if it is.
- name: Clearing tmpfs mount for /var/lib/ldap
  lineinfile:
    path: "/etc/fstab"
    regexp: "^none /var/lib/ldap"
    state: absent
  when: float_ldap_is_master

# Actually mount /var/lib/ldap if it's not mounted (like on the first
# execution, it will be mounted at the next reboot).
- name: Mount /var/lib/ldap
  shell: "grep -q '^none /var/lib/ldap' </proc/mounts || mount /var/lib/ldap"
  changed_when: False
  when: "not float_ldap_is_master"

- name: Umount /var/lib/ldap
  shell: "grep -q '^none /var/lib/ldap' </proc/mounts && umount /var/lib/ldap || true"
  changed_when: False
  when: float_ldap_is_master

# Now that everything is ready (configuration and database dirs), and
# most importantly having set the slapd/no_configuration debconf
# variable, we can install the package. The installation shouldn't
# have any side effects (such as dumping the database, or creating
# /etc/ldap/slapd.d).
- name: Install the LDAP packages
  apt:
    name: "{{ packages }}"
    state: present
  vars:
    packages:
      - slapd
      - slapd-prometheus-exporter
      - ldap-utils

- name: Install our slapd systemd service
  template:
    src: "slapd.service.j2"
    dest: "/etc/systemd/system/slapd.service"
  register: slapd_service

- name: Install ldap-wait-syncrepl
  copy:
    src: ldap-wait-syncrepl
    dest: /usr/sbin/ldap-wait-syncrepl
    mode: 0555
    owner: root
    group: root

- name: Install ldap synchronization units
  copy:
    src: "{{ item }}"
    dest: "/lib/systemd/system/{{ item }}"
    mode: 0444
    owner: root
    group: root
  with_items:
    - ldap-online.target
    - ldap-wait-syncrepl.service
  register: ldap_online

- name: Start ldap synchronization units
  systemd:
    name: "{{ item }}"
    state: started
    masked: no
    enabled: yes
    daemon_reload: yes
  with_items:
    - ldap-online.target
    - ldap-wait-syncrepl.service
  when: "ldap_online is changed"

# We can't use a handler because the next step requires slapd to be
# running.  Also we need to use 'restart', because on a first install
# at this stage the systemd service thinks the old sysvinit script has
# executed successfully (exiting due to SLAPD_NO_START) and it will
# report the unit as active.
- name: Start slapd
  systemd:
    name: slapd.service
    state: restarted
    masked: no
    enabled: yes
    daemon_reload: yes
  when: "slapd_service is changed"

- include_tasks: bootstrap.yml
  when: float_ldap_is_master
