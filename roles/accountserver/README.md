accountserver
===

This role configures the
[accountserver](https://git.autistici.org/ai3/accountserver), the main
API for account manipulation. Applications in ai3 do not talk directly
to the LDAP database, but go through the accountserver API to fetch
and modify account information.

The accountserver scales horizontally with a leader/follower model:
all updates are sent to the leader, but fetches can be served by any
of the followers.

Some API calls require further authentication (the user's primary
password) in order to manipulate account credentials.

## Request flow

![RPC Overview](rpc.png "RPC Overview")

