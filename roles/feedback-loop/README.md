Role that configures our [SPAM Feedback Loop reporting
service](https://git.autistici.org/ai3/tools/feedback-loop).
