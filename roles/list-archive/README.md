Role to install *lurker*, a mailing list archiver for public mailing
lists. This acts as the public archiver for our *mailman* service: the
float service *list-archive* includes an internal SMTP API.

## Usage of the internal SMTP API

In order for mailman to talk to the list-archive SMTP API, we need to
configure a route for it in Postfix. Since float (Ansible, actually)
can't define parameters for other roles to consume, we have to rely on
the top-level configuration variable *mail_transport_map*. It will
need to include the following:

```yaml
mail_transport_map:
  list-archive.investici.org: "relay:[list-archive.investici.org]:8067"
```

And then we can tell mailman to send emails to the
*list-archive.investici.org* domain.
