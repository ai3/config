accounts
===

This role sets up the [user
panel](https://git.autistici.org/ai3/pannello), a web application that
is the primary way that users interact with our services.

The application can easily scale horizontally, as all dependencies are
mediated by RPCs:

* the authentication workflow depends on SSO
* the user database is provided by the
  [accountserver](https://git.autistici.org/ai3/accountserver)
* the (tiny) application state data is stored in client-side session
  cookies, signed and encrypted

## Request Flow

![RPC Overview](rpc.png "RPC Overview")

In the flow diagram above, requests are made by the user's browser
via HTTP to the accounts.autistici.org web application ("pannello").

* Black edges represent read-only requests to critical backends: if
  these fail, the application returns an error.

* Red edges represent mutation requests (resource management, etc):
  if these fail, only the associated functionality is broken (say,
  the user is unable to change their password etc).

* Grey edges represent non-critical read-only requests: these can
  fail silently and the application will handle this by reducing its
  functionality -- generally by not displaying certain additional
  information.

