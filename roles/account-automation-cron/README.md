Set up a cron job from
[ai-scripts](https://git.autistici.org/ai/ai-scripts), using a systemd
timer.

This role relies on the ai-scripts package creating all the *aa-\**
systemd services (not enabled by default), this role will just create
corresponding *.timer* script. It is meant to be included by other
roles, either directly or via *meta/main.yml*.

It expects to be invoked with a *scripts* Ansible variable, which is a
list of cron jobs to set up. Each object should have a *name* (without
the "aa-" prefix) and a *calendar* attribute describing the desired
schedule in the systemd cron format.
