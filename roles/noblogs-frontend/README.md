Small role that runs on *frontend* nodes and installs the noblogs.org
DNS configuration, which is special.

We can't use the *noblogs* role for this as that one does not
necessarily run on frontend hosts.
