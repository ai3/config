rsync
===

This role is used to move user data between backends, using rsync.

Connections are authenticated using a shared secret.
