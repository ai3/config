This role installs
[ai-scripts](https://git.autistici.org/ai/ai-scripts) cron jobs that
are associated with the *mail* service and run on the *backend* nodes.

These include the various *create*/*update*/*move-maildirs* scripts.


