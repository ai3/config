This role is a dependency of all other *account-automation-\** roles,
and it prepares the system for running the
[ai-scripts](https://git.autistici.org/ai/ai-scripts) suite of account
automation tools.

Most importantly, it installs the */etc/ai/account-automation.json*
file which configures the ai-scripts tools.
