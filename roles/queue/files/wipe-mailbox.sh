#!/bin/bash

confirm=0
mailbox=
while [ $# -gt 0 ]; do
    case "$1" in
        --yes-i-am-sure)
            confirm=1
            ;;
        -*)
            echo "Unkown option $1" >&2
            exit 2
            ;;
        *)
            # First argument is the mailbox name.
            if [ -n "${mailbox}" ]; then
                echo "Too many arguments ">&2
                exit 2
            fi
            mailbox="$1"
            ;;
    esac
    shift
done

if [ -z "${mailbox}" ]; then
    echo "Usage: $0 <mailbox>" >&2
    exit 2
fi

if [ $confirm -ne 1 ]; then
    echo "You did not pass the confirmation option --yes-i-am-sure!" >&2
    exit 1
fi

set -euo pipefail

# Get the list of mailboxes in an array.
readarray -t mailbox_list < <(doveadm mailbox list -u ${mailbox} 2>&1)

# Expunge each one.
for m in "${mailbox_list[@]}"; do
    doveadm mailbox expunge -u "${mailbox}" mailbox "${m}" all
done

doveadm purge -u "${mailbox}"

exit 0
