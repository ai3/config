Queue manager
===

This role configures [enq](https://git.autistici.org/ai3/tools/enq), a
simple distributed (partitioned) task queue manager, which we use for
its ability to run data-dependent tasks on-demand close to the
location of the data itself.

In the A/I context, these tasks are usually related to account
management. For a lot of asynchronous operations, where there is no
need for an immediate result, it's easier to use this system than to
set up a dedicated RPC server.
