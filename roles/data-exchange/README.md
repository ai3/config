data-exchange
===

This role runs a simple static HTTP server, open to all, as the
"data-exchange" public endpoint. Its purpose is to serve encrypted
datasets to services external to ai3, using simple symmetric
encryption (so a shared-secret scheme).

The encryption implementation uses GPG and it can be found in
[ai/ai-scripts](https://git.autistici.org/ai/ai-scripts/blob/master/ai/data_exchange.py).

To use it, put the shared credentials in the variable
*data_exchange_credentials*, a list of items with *name* and *secret*
attributes.
