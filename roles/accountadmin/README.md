accountadmin
===

This role configures the [account management web
application](https://git.autistici.org/ai3/accountadmin).

This is an admin-privileged web application that implements SSO
authentication itself.

## Request flow

![RPC Overview](rpc.png "RPC Overview")

Accountadmin calls a number of backends to gather information, but all
modifications to the user database go through the *accountserver*.
