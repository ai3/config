This role installs
[ai-scripts](https://git.autistici.org/ai/ai-scripts) cron jobs that
are associated with the *web-users* service and run on the *backend*
nodes.

These include:

* various *create*/*update*/*move-websites* scripts
* *config-apache-instances* to set up Apache
* other website-related scripts
