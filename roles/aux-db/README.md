Role that configures
[aux-db](https://git.autistici.org/ai3/tools/aux-db), a partitioned
SQL db for simple user-oriented data, that runs on all our backend
nodes.
