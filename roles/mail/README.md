mail
====

This role installs the full email service stack: Postfix, Dovecot, and
all the associated daemons.

It sets up the *mail-frontend* and the *mail-backend* services: they
have so much in common that we handle them both from a single Ansible
role, using the *float_enable_SERVICE* variables to toggle specific
behavior.

# Postfix and message routing

In the previous architecture, we had a very complex master.cf
configuration, with wildly different options set on different smtpd
endpoints. That was very difficult to understand. So instead here we
are setting up multiple Postfix instances (a process that is now
entirely straightforward in Debian stretch), each one of them with a
much simpler and cleaner configuration. The results so far look
encouraging.

## Quick overview of multi-instance support in Postfix

For context and background information, it's worth reading the
[MULTI\_INSTANCE\_README](http://www.postfix.org/MULTI_INSTANCE_README.html)
from the official Postfix documentation.

Each Postfix instance has a different configuration directory, and
uses different file-based maps, except for the LDAP maps which in our
implementation are shared between all instances and can be found in
*/etc/postfix/ldap*.

Names of Postfix instances must start with `postfix-`, with the
special exception of the default instance which is simply named
`-`. You can see a list of which instances are configured on a server
by running:

```shell
$ postmulti -l
```

On Debian stretch, and other systemd-based systems, the systemd units
for Postfix instances are automatically instantiated and are named
`postfix@INSTANCE_NAME`: so we have *postfix@-* for the default
instance, *postfix@postfix-in* for an instance named *postfix-in*,
etc.

## Postfix instances

On the *mail-frontend* hosts, the following instances are running:

* *-*, the default instance, does not listen on any SMTP ports but
  just forwards mail received via the system-wide */usr/sbin/sendmail*
  program over to the *postfix-out* instance.
* *postfix-in* listens on port 25 on the public IP addresses of the
  server, receiving email for our users from external mail
  servers. This instance has a number of policies on incoming traffic,
  including RBL-based blacklists, strict SPF checks, etc.  All
  received emails will be forwarded to the *postfix-out* instance for
  internal delivery. Messages for unknown recipients will be rejected
  right away, to avoid having to deliver bounces (backscatter).
  Runs the following policy service:
  * policyd-spf
* *postfix-smtp-auth* receives emails from authenticated clients over
  SMTP, and delivers them to *postfix-out*. This instance does not
  reject unknown recipients: if the recipient does not exist,
  *postfix-out* will generate a bounce, which is fine in this case
  because we know that the sender is internal and exists. Runs the
  following milters:
  * policyd-rate-limit
* *postfix-out* delivers emails, both externally and to internal users
  (to *mail-backend* servers). It signs outbound emails with
  DKIM. Internal email addresses are resolved to their final
  recipients (aliases, etc), and delivered to the correct backend
  server according to the recipient shard. Runs the following milters:
  * opendkim

On *mail-backend* servers, the situation is simpler:

* *-* behaves just like above.
* *postfix-delivery* receives incoming email from *postfix-out*,
  addressed to individual recipients, applies personalized spam
  filtering, and finally delivers the message to Dovecot over LMTP.
  Runs the following milters:
  * spamass-milter (integrates with Spamassassin)
* *postfix-webmail* receives authenticated SMTP submissions from the
  webmail, applies rate limits, and forwards emails for delivery to
  *postfix-out*. Runs the following milters:
  * policyd-rate-limit

![Email flow overview](flow.png "Email flow")

Note that emails to the Mailman service are delivered directly via
LMTP, from the *postfix-in* instance, so that eventualy rejections
from Mailman happen as soon as possible without us ever accepting the
message: this removes most of the moderation backscatter spam. There
is also another Spamassassin check inline before Mailman.

### DKIM

There is some special treatment for solving the problem of signing
DKIM emails only from "good" senders, that we have resolved without
adding yet another Postfix instance. The *postfix-out* instance
actually has two smtpd listeners: one (port 10025) to be used by all
"trusted" clients (internal ones, *postfix-smtp-auth*, etc), and a
different one (port 10026) used only by *postfix-in* for all untrusted
email. 

This latter smtpd listener does not have the opendkim milter
configured, so we can trigger DKIM signing by actually looking at the
message delivery flow, rather than simply at the *From* address (which
is unreliable).

# Dovecot and user mailbox storage

Inbound IMAP/POP connections follow a similar flow to messages being
delivered: on *mail-frontend* hosts, Dovecot acts as a reverse proxy,
while on *mail-backend* hosts it manages the user mailboxes.

When Dovecot runs as a proxy, it does not need to authenticate users
at all: it just needs to retrieve from LDAP the shard that the user is
assigned to, so it knows which back-end to connect to.

Dovecot on back-ends runs in a fairly standard mdbox-based setup, with
a few fancy features:

* authentication via [auth-server](https://git.autistici.org/id/auth)
  using the [PAM bridge](https://git.autistici.org/id/auth-pam)
* support for per-user mailbox encryption using [Dovecot's own
  mail_crypt plugin](https://wiki.dovecot.org/Plugins/MailCrypt), with
  Argon2 as a KDF and support for multiple passwords per user. This is
  handled by [keystore](https://git.autistici.org/id/keystore).

# User Authentication

All services authenticate users by talking to the
[auth-server](https://git.autistici.org/id/auth). Due to the specifics
of each application, there are various methods used to do so:

* dovecot uses the [auth-client PAM
  module](https://git.autistici.org/id/auth-pam). Since we are using the
  [Director](https://wiki2.dovecot.org/Director) as a connection proxy,
  user authentication always happens on the backends.
* dovecot also authenticates users using SSO directly (still via PAM),
  for requests coming from the webmail.
* postfix (the *postfix-smtp-auth* instance) authenticates client SMTP
  submissions using a [custom SASL
  server](https://git.autistici.org/id/auth-sasl-server) that talks
  directly to the auth-server. We had to write this one due to
  limitations with *saslauthd*.
* postfix also needs to accept authenticated SMTP submissions from
  webmail clients (the *postfix-webmail* instance), which use another
  instance of auth-sasl-server configured to verify SSO tickets.

# Notes

## Caveats on multi-instance Postfix

There are a few tricky things to keep in mind when working with
multiple Postfix instances that talk to each other:

* Reject messages as soon as possible (i.e. in *postfix-in*), to
  prevent generating bounces on our side.

* You don't want messages to be queuing anywhere except on the
  outbound instance, deferreds on other instances are only acceptable
  as a temporary consequence of internal service failures.

* Some instances are going to have to share data: either make it so
  the Ansible roles autogenerate it using templates, or be prepared to
  some duplication.

* We have chosen to keep all the routing logic in a single instance
  (*postfix-out*), which means that we need to follow a star topology:
  all messages, even internal ones, have to go through that instance
  once.

# Testing

The test environments (that is, unless you explicitly set *testing* to
False) enable the [XCLIENT SMTP
extension](http://www.postfix.org/XCLIENT_README.html) to allow you to
assume the identity of an arbitrary client host. Without this, it
would be impossible to test SMTP functionality as all requests would
fail SPF and other IP-level checks.

# Playbook

## Custom email routing

The Postfix configuration in this role is meant to provide email
service for our LDAP-based user accounts and mailing lists. But there
is anyway the necessity to provide custom email routes for particular
addresses, and this role provides a small number of configuration
parameters to do so.

* `mail_alias_map` is a dictionary of email / destination addresses to
  support aliasing. The destination addresses will be routed normally.
* `mail_forward_map` is a dictionary of email / transport that is used
  to transparently redirect emails to specific external
  servers. Transports use the Postfix transport(5) target syntax.
* `mail_forward_pcre_map` is a PCRE version of the above, for
  wildcards.
* `mail_transport_map` is an email / transport map for *outbound*
  delivery (see also the *Delivering to programs* section below).
  Note that addresses in this map are not automatically added to the
  list of allowed incoming emails (i.e. they must already exist).

## Working with multiple Postfix instances

With multiple Postfix instances, you can't run the *postfix* wrapper
command anymore: instead, you should use the *postmulti* command.
For instance, what used to be a simple

```shell
$ postfix flush
```

should now become:

```shell
$ postmulti -p flush
```

which will flush the queue of messages on all instances. To run
commands on a specific instance only, pass the instance name with the
*-i* command-line flag to *postmulti*, e.g.:

```shell
$ postmulti -i postfix-out -p flush
```

The above applies to commands that one would normally run via the
*postfix* wrapper. For other standalone utilities like *mailq*, one
should use the *-x* option to *postmulti* instead of *-p* like in the
above examples. So for instance where one would normally have used

```shell
$ mailq
```

now one should say:

```shell
$ postmulti -x mailq
```

to see messages queued in every instance, or:

```shell
$ postmulti -i postfix-out -x mailq
```

for a specific instance. Check the *postmulti(1)* man page for further
details.

## Disabling outbound IPv6 on a host

Sometimes there are problems with reverse IPv6 DNS resolution of one
of our public MX addresses. In this case outbound email might be
refused by most email servers that do a reverse DNS check of the EHLO
name. The solution is to set the special host variable
*postfix_out_inet_protocols* on the specific server (in the
inventory file) to the value "ipv4".

## Delivering to programs

First of all, the old traditional way of pipe-email-to-program is just
not possible or practical in a containerized environment: email
ingestion software is expected to have its own SMTP interface,
possibly via [minimal SMTP daemons built for this explicit
purpose](https://git.autistici.org/ai3/tools/smtpd-pipe).

To add a custom (that is, not already generated by the automation)
delivery rule for an email address, that ultimately delivers email to
a program, you are going to need two pieces of information:

* the public email address
* an internal name for the service
* a target for the delivery, an SMTP server somewhere represented in
  the usual *host:port* form. If the target is a
  [float](https://git.autistici.org/ai3/float) service, the service
  name is normally used as the hostname.

With this, we're going to configure the system so that it (optionally)
receives email on the public address, and forwards it to the desired
internal service.

Let's take for example a hypothetical *helpdesk* service, that
receives emails (with its own SMTP server) on
*helpdesk.internal.example.com:8025*. The information above needs to
be converted into two forms:

* a *transport(5)* Postfix map entry that sends the email address to
  the target, using the *relay* handler. For instance:

```
helpdesk.internal.example.com    relay:[helpdesk.internal.example.com]:8025
```

* a *virtual(5)* Postfix map, if you want this address to be reachable
  from the outside, to rewrite the public email address to the internal one:

```
help@example.com    help@helpdesk.internal.example.com
```

You should then add these value pairs to the *mail_alias_map* and to
the *mail_transport_map* configuration variables respectively.

## Finding compromised accounts (manually)

Manual inspection of the mail queues can sometimes help in finding
compromised accounts. Often, many of the addresses in the spammers'
lists have temporary delivery errors, and the messages stay in our
queues. By looking at queue statistics it's relatively easy to spot
these accounts, possibly even manually inspecting the messages for
positive confirmation. One distinctive aspect is that they generally
tend to send messages to large email providers (gmail, hotmail,
yahoo), much more so than normal users.

Once you are logged in on a *frontend* server, a one-liner to get
statistics about the senders of the messages in the *postfix-out* mail
queue is the following:

```shell
$ postmulti -i postfix-out -x mailq | awk '/^[0-9A-Z]/ {print $7}' \
    | egrep -v 'bounces|MAILER-DAEMON' | sort | uniq -c | sort -n | tail -20
```

Then one can inspect the queue for messages from a specific user, to
see their queue IDs and the recipients:

```shell
$ postmulti -i postfix-out -x mailq | grep -A 2 user@example.com
```

If the patterns look suspicious, manual inspection of specific
messages is possible with:

```shell
$ postmulti -i postfix-out -x postcat -q 9E41B120730
```

If the messages look like spam, disable the account.
