---

# Install a generic Postfix instance.

# The variable 'postfix_instance' must be defined as the current loop
# control variable, and must contain a 'name' attribute.

- set_fact:
    postfix_instance_name: "{{ postfix_instance.name }}"

- set_fact:
    postfix_suffix: "{% if postfix_instance_name %}-{% endif %}{{ postfix_instance_name }}"
    postfix_dir: "/etc/postfix{% if postfix_instance_name %}-{% endif %}{{ postfix_instance_name }}"
    postfix_systemd_service: "postfix@{% if postfix_instance_name %}postfix-{{ postfix_instance_name}}{% else %}-{% endif %}.service"

- stat:
    path: "{{ postfix_dir }}"
  register: postfix_dir_stat

# We run postmulti, which will modify the main.cf file, which sucks.
- name: Run postmulti to initialize Postfix instance
  command: postmulti -I "postfix-{{ postfix_instance_name }}" -G "{{ postfix_instance_group | default('mta') }}" -e create
  when: "( not postfix_dir_stat.stat.exists ) and ( postfix_instance_name != '' )"
  changed_when: false

# In order to avoid destructive editing of main.cf, fetch
# the current value of multi_instance_directories.
- shell: "test -e /etc/postfix/main.cf && postconf -x -h multi_instance_directories"
  when: "postfix_instance_name == ''"
  register: postfix_multi_instance_directories
  check_mode: no
  changed_when: false

- name: Install Postfix config (dirs)
  file:
    path: "{{ postfix_dir }}/{{ item.path }}"
    state: directory
  with_filetree: "templates/postfix{{ postfix_suffix }}/"
  when: item.state == 'directory'

- name: Install Postfix config (files)
  template:
    src: "{{ item.src }}"
    dest: "{{ postfix_dir }}/{{ item.path }}"
  with_filetree: "templates/postfix{{ postfix_suffix }}/"
  when: item.state == 'file'
  notify: "reload postfix{{ postfix_suffix }}"
  vars:
    final_transport_map: "{{ mail_transport_map | combine(host_transport_map | default({})) }}"

- name: Touch autogenerated Postfix config files
  file:
    state: touch
    path: "{{ postfix_dir }}/{{ item }}"
  changed_when: false
  with_items:
    - 'access-sender'

- name: Regenerate all Postfix hash maps
  shell: "postconf -c {{ postfix_dir }} -x | tr ' ,' '\\n' | perl -nle 'print $2 if /^(hash|cdb):([^ ,]+)/' | sort -u | grep -v /\\$ | xargs --no-run-if-empty -n 1 /usr/local/bin/postmap-create-if-missing"
  changed_when: false

- name: Force postfix domains regeneration
  shell: "if test -e /etc/postfix/domains; then postmap /etc/postfix/domains; fi"
  changed_when: false

- name: Enable systemd unit {{ postfix_systemd_service }}
  systemd:
    name: "{{ postfix_systemd_service }}"
    enabled: yes
    masked: no
  notify: "reload postfix{{ postfix_suffix }}"

# Skip on first initialization, postfix@- is still listening on port 25
# and starting the additional instances now will result in a conflict.
- name: Ensure systemd unit {{ postfix_systemd_service }} is running
  systemd:
    name: "{{ postfix_systemd_service }}"
    state: started
  when: "postfix_dir_stat.stat.exists"

