<?php

/*
 +-----------------------------------------------------------------------+
 | Local configuration for the Roundcube Webmail installation.           |
 |                                                                       |
 | This is a sample configuration file only containing the minumum       |
 | setup required for a functional installation. Copy more options       |
 | from defaults.inc.php to this file to override the defaults.          |
 |                                                                       |
 | This file is part of the Roundcube Webmail client                     |
 | Copyright (C) 2005-2013, The Roundcube Dev Team                       |
 |                                                                       |
 | Licensed under the GNU General Public License version 3 or            |
 | any later version with exceptions for skins & plugins.                |
 | See the README file for a full license statement.                     |
 +-----------------------------------------------------------------------+
*/

$config = array();

// Database connection string (DSN) for read+write operations
// Format (compatible with PEAR MDB2): db_provider://user:password@host/database
// Currently supported db_providers: mysql, pgsql, sqlite, mssql or sqlsrv
// For examples see http://pear.php.net/manual/en/package.database.mdb2.intro-dsn.php
// NOTE: for SQLite use absolute path: 'sqlite:////full/path/to/sqlite.db?mode=0646'
$config['db_dsnw'] = 'mysql://roundcube:{{ roundcube_db_password }}@127.0.0.1:3308/ai_roundcube?charset=utf8mb4';

// The mail host chosen to perform the log-in.
// Leave blank to show a textbox at login, give a list of hosts
// to display a pulldown menu or set one host as string.
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
// Supported replacement variables:
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %s - domain name after the '@' from e-mail address provided at login screen
// For example %n = mail.domain.tld, %t = domain.tld
$config['imap_host'] = 'localhost:10143';
// TODO: remove
$config['default_host'] = 'localhost';
$config['default_port'] = 10143;

// SMTP server host (for sending mails).
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
// If left blank, the PHP mail() function is used
// Supported replacement variables:
// %h - user's IMAP hostname
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %z - IMAP domain (IMAP hostname without the first part)
// For example %n = mail.domain.tld, %t = domain.tld
$config['smtp_host'] = 'localhost:10025';
// TODO: remove
$config['smtp_server'] = '127.0.0.1';
$config['smtp_port'] = 10025;

// SMTP username (if required) if you use %u as the username Roundcube
// will use the current username for login
$config['smtp_user'] = '%u';

// SMTP password (if required) if you use %p as the password Roundcube
// will use the current user's password for login
$config['smtp_pass'] = $_SERVER['SSO_TICKET'];

// Other SMTP parameters.
$config['smtp_auth_type'] = 'PLAIN';
$config['smtp_helo_host'] = '{{ shard_id }}.webmail.{{ domain }}';

// SMTP connection timeout, in seconds. Default: 0 (use default_socket_timeout)
// Note: There's a known issue where using ssl connection with
// timeout > 0 causes connection errors (https://bugs.php.net/bug.php?id=54511)
$config['smtp_timeout'] = 0;

// SMTP socket context options
// See http://php.net/manual/en/context.ssl.php
// The example below enables server certificate validation, and
// requires 'smtp_timeout' to be non zero.
// $config['smtp_conn_options'] = array(
//   'ssl'         => array(
//     'verify_peer'     => true,
//     'verify_depth     => 3,
//     'cafile'          => '/etc/ai/ca.pem',
//   ),
// );

// Name your service. This is displayed on the login screen and in the window title
$config['product_name'] = 'A/I Webmail';

// this key is used to encrypt the users imap password which is stored
// in the session record (and the client cookie if remember password is enabled).
// please provide a string of exactly 24 chars.
// YOUR KEY MUST BE DIFFERENT THAN THE SAMPLE VALUE FOR SECURITY REASONS
$config['des_key'] = '{{ roundcube_secret }}';
//$config['cipher_method'] = 'AES-256-CBC';

// skin name: folder from skins/
$config['skin'] = 'elastic';
$config['skin_logo'] = '/images/r_logo.png';

// Force all users to use the same skin to avoid
// having to support obsolete skins forever.
$config['dont_override'] = array('skin');

// Session cookie settings.
$config['session_domain'] = '';
$config['session_path'] = '/';
$config['session_lifetime'] = 60;

// Password charset.
// Use it if your authentication backend doesn't support UTF-8.
// Defaults to ISO-8859-1 for backward compatibility
$config['password_charset'] = 'UTF-8';

// Use this charset as fallback for message decoding
$config['default_charset'] = 'UTF-8';

// Use the system-wide mime.types file.
$config['mime_types'] = '/etc/mime.types';

// Do not leak the Roundcube version.
$config['useragent'] = 'Roundcube Webmail';

// Do not save drafts to browser's localstorage
$config['compose_save_localstorage'] = false;

// Display these folders separately in the mailbox list.
// these folders will also be displayed with localized names
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['default_folders'] = array('INBOX', 'Drafts', 'Sent', 'Spam', 'Trash');

// Automatically create the above listed default folders on first login
$config['create_default_folders'] = true;

$config['junk_mbox'] = 'Spam';

// Disable the build-in spell checker.
$config['enable_spellcheck'] = false;

// IMAP protocol configuration, Dovecot-specific.
// Most of these are optimizations so Roundcube can skip auto-detection of
// some features.
$config['imap_timeout'] = 10;
$config['imap_vendor'] = 'dovecot';
$config['imap_force_caps'] = true;
$config['imap_dual_use_folders'] = true;

// List of active plugins (in plugins/ directory)
$config['plugins'] = array(
    'archive',
    'csp',
    'hide_blockquote',
    'managesieve',
    'markasjunk',
    'newmail_notifier',
    'sauserprefs',
    'sso',
    'zipdownload',
);

// Logging configuration.
$config['log_driver'] = 'syslog';
$config['syslog_id'] = 'roundcube';
$config['syslog_facility'] = LOG_MAIL;

// Proxies to trust with the X-Forwarded-Proto header. To our Apache, all
// addresses look like localhost. This makes Roundcube detect HTTPS properly.
$config['proxy_whitelist'] = array(
    '127.0.0.1',
    '::1',
);

// Do not prefer HTML messages
$config['prefer_html'] = false;

// Clear Trash and expunge INBOX on logout.
$config['logout_purge'] = true;
$config['logout_expunge'] = true;

// Reply-all goes to list if mailing list is detected.
$config['reply_all_mode'] = 1;
