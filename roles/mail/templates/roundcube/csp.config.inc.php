<?php

$config['csp_script_path'] = '/';
$config['csp_sso_hostname'] = 'https://accounts.autistici.org';
$config['csp_report_to'] = 'default';
// Necessary until Firefox supports report-to.
$config['csp_report_uri'] = 'https://live-reports.autistici.org/ingest/v1';

?>
