# Postfix configuration file for the postfix-webmail instance.
# Receives user submissions via authenticated SMTP (no TLS, we are
# only listening on localhost).

{% include "main.cf.base.j2" %}

pcre = pcre:${config_directory}/
ldap = proxy:ldap:/etc/postfix/ldap/

# No local delivery
mydestination =
alias_maps =
alias_database =
local_recipient_maps =
local_transport = error:5.1.1 Mailbox unavailable

# Don't rewrite remote headers
local_header_rewrite_clients =

# All recipients of not yet filtered email go to the postfix-out
# instance together.
#
# With multiple instances, the content-filter is specified
# via transport settings not the "content_filter" transport
# switch override! Here the filter listens on local port 10025.
#
# If you need to route some users or recipient domains directly to the
# output instance bypassing the filter, just define a transport table
# with suitable entries.
default_transport = smtp:[mail-frontend.{{ domain }}]:10025
relay_transport = $default_transport
virtual_transport = $default_transport
transport_maps =

# Pass original client log information through the filter.
smtp_send_xforward_command = yes

# SASL auth configuration.
smtpd_sasl_auth_enable = yes
smtpd_sasl_security_options = noanonymous
smtpd_sasl_local_domain = $mydomain
smtpd_sasl_authenticated_header = yes
broken_sasl_auth_clients = yes
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth

# Apply the policy limit and sender login control.
smtpd_recipient_restrictions =
    reject_non_fqdn_recipient,
    reject_sender_login_mismatch,
    check_sender_access ${indexed}access-sender,
    check_policy_service { unix:ratelimit/policy, default_action=DUNNO },
    permit_sasl_authenticated,
    reject

# Maps defining who can send as who.
smtpd_sender_login_maps =
    ${pcre}sender-login,
    ${ldap}sender-login

# Use /etc/hosts to lookup names.
smtp_host_lookup = native
