#!/bin/bash
#
# Filter out messages matching a specific criteria.
#

usage() {
    cat >&2 <<EOF
Usage: pqgrep [<options>] <sender>
Known options:
  -i INSTANCE       Limit to a specific Postfix instance
  -d                Delete the selected messages from the queue

EOF
    exit 2
}

instances=postfix-out
do_delete=0
sender=
while [ $# -gt 0 ]; do
    case "$1" in
        -i)
            instances="$instances $2"
            shift
            ;;
        -d)
            do_delete=1
            ;;
        -h|--help)
            usage
            ;;
        -*)
            echo "Unknown option '$1'" >&2
            usage
            ;;
        *)
            if [ -n "$sender" ]; then
                echo "Too many arguments" >&2
                usage
            fi
            sender="$1"
            ;;
    esac
    shift
done

[ -n "$sender" ] || usage

if [ "$instances" = "all" ]; then
    instances=$(postmulti -l | awk '$1 != "-" {print $1}')
fi

for instance in $instances; do
    cmd="cat"
    if [ $do_delete -eq 1 ]; then
        cmd="xargs -n 1 postmulti -i $instance -x postsuper -d"
    fi
    postmulti -i $instance -x postqueue -j \
        | jq -r "select(.sender == \"${sender}\") | .queue_id" \
        | $cmd
done
