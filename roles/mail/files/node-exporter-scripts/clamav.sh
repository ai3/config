#!/bin/sh

base="/var/lib/clamav"

test -d "$base" || exit 0

files="urlhaus.ndb daily.cld"

for file in ${files}; do
    path="${base}/${file}"
    [ -e "$path" ] || continue
    mtime="$(stat --format %Y ${path})"
    echo "clamav_database_update_timestamp{db=\"${file}\"} ${mtime}"
done

exit 0
