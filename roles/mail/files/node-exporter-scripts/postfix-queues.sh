#!/bin/sh

test -x /usr/sbin/postmulti || exit 0

queues="active deferred incoming hold"
instances=$(/usr/sbin/postmulti -l | awk '{print $1}')

for instance in ${instances}; do
    if [ "x${instance}" = "x-" ]; then
        instance=postfix
    fi
    spool_dir=/var/spool/${instance}
    for queue in ${queues}; do
        test -d ${spool_dir}/${queue} || continue
        n=$(find ${spool_dir}/${queue} -type f -print | wc -l)
        echo "postfix_queue_length{postfix_instance=\"${instance}\",queue=\"${queue}\"} ${n}"
    done
done

exit 0
