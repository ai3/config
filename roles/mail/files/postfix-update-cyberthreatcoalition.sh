#!/bin/sh
#
# Update the COVID-19 blacklist from cyberthreatcoalition.org
#

umask 022

TMP=$(mktemp /tmp/corona.XXXXXXXX)
curl -sL https://blacklist.cyberthreatcoalition.org/vetted/domain.txt \
  | tr -d '\r' \
  | grep -v '^#' \
  | sed -e 's/$/\tREJECT\tCorona scam/' \
  > "${TMP}"
[ -s "${TMP}" ] && \
  chmod 644 "${TMP}" && \
  /bin/mv "${TMP}" /etc/postfix-in/access-sender-ctc && \
  /usr/sbin/postmap /etc/postfix-in/access-sender-ctc
/bin/rm "${TMP}" 2>/dev/null

