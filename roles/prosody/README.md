jabber: prosody
===

Ansible role that installs our [containerized prosody](https://git.autistici.org/ai3/docker/prosody)
and configures it.

It is meant to run on backends, with connections
forwarded via *haproxy*, so it terminates SSL on its own (this is necessary
because haproxy doesn't speak its custom STARTTLS protocol). In order
to do this, it depends on the *acme-storage* Ansible role, which sets
up the *replds@acme* service. This pulls in a copy of /etc/credentials/public.

The replds service must be reflected in the *services.yml* configuration
which is why you can see in *services.im.yml* some service_credentials
named 'replds-acme', and a corresponding systemd_service.

To configure it, define the list of domains it should support in the
