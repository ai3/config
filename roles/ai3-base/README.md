Customizations over the float "base" role. These are things that we
want to set up identically on **all** machines.

The list of customizations include:

* creating a tracing configuration pointing at our Jaeger service
* add some additional auditd rules that are relevant to our services

And some additional tasks that are only performed on bare-metal
servers:

* set up dropbear-initramfs configuration
* install some hardware-focused packages that are useful for debugging
* set up SMART monitoring
