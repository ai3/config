# noblogs

This role sets up noblogs.org, using
[noblogs/noblogs-composer](https://git.autistici.org/noblogs/noblogs-composer)
as its main container image.

We also run memcached along with it, to use both as session cache and
object cache.

This role takes care of setting up the database layer, using the
*float-util-mariadb* role to configure MySQL in our partitioned setup:

* the *noblogs* database, which holds global data such as lists of
  users and blogs, is replicated in a master/slave configuration
* the various blog-specific tables are partitioned in databases called
  *noblogs_N* which are local to each server (*N* is just the
  *shard_id* of the server)

A Wordpress database plugin takes care of making sure that PHP is
talking to the right database, and can do split read/write global
reads so as to take advantage of the read-only slave replicas of the
main db.

The role creates the configuration for the PHP application, including
the database config, in */etc/noblogs/config.json*.
