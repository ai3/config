#!/bin/sh
#
# Set up the CI environment with a persistent SSH key
# stored in $BUILD_DIR, which can survive between 
# different stages via artifacts.

key_dir="${BUILD_DIR}/ssh"
key_path="${key_dir}/vmkey"
config_dir="/root/.ssh"
config_path="${config_dir}/config"

set -e

# Generate the key if it does not exist.
if [ ! -e "${key_path}" ]; then
    mkdir -p "${key_dir}"
    chmod 0700 "${key_dir}"
    ssh-keygen -t ed25519 -N '' -C '' -f "${key_path}"
    chmod 0600 "${key_path}"
fi

# Configure SSH to use it for the 10/8 network
# (the network used by vmine).
mkdir -p "${config_dir}"
chmod 0700 "${config_dir}"
cat >> "${config_path}" <<EOF
Host 10.*
    User root
    IdentityFile ${key_path}
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
EOF

exit 0
