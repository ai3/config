Coding and contribution guidelines
===

This document contains a random collection of thoughts and principles
about coding practices and style common in this and other ai3
repositories.

# Ansible Practices

## Where to put my new Ansible role?

Most definitely in the
[ai3/config](https://git.autistici.org/ai3/config) repository.

There are however a couple of exceptions:

* Large datasets that only need to apply to the production
  environment, that must be private, and that would not be practical
  to parameterize as Ansible configuration parameters. Examples:
  * custom DNS zones (boum.org, etc.)
* Roles that need to be private, because we don't want the world to
  know that (or how) we're running certain services.

## Storing secrets

A secret is any piece of data (passphrase, certificate, encryption
key, whatever) that must not be published. There are three types of
secrets:

* Third-party secrets, which we can't change easily, are common to all
  environments (test, prod) and belong to third-party services. These
  must be encrypted with GPG, and shared via the
  [ai3/pass](https://git.autistici.org/ai3/pass) repository.
* *For the moment* (until issue #46 is resolved), since we lack a
  decent solution to avoid lots of manual copy&paste, these secrets
  can be stored encrypted with *ansible-vault* in the *group_vars/all*
  directory in this repository. For instructions on how to do so, see
  the [README](README.md#adding-a-secret-to-the-repository).
* Credentials that are self-contained within the ai3 environment (all
  internal credentials, for instance) can be automatically generated
  by Ansible: you just need to describe them in one of the
  *passwords.yml* files
  ([documentation](https://git.autistici.org/ai3/float/blob/master/docs/ansible.md#automated-credentials-management)).

## My service needs to add some customization to the frontends

It is sometimes the case that certain services need to customize the
configuration of the *float* front-end services (HTTP, DNS): imagine
for instance having to add a new DNS zone, or a NGINX configuration
snippet.

In this case it's impossible to run this customization from the same
Ansible role that configures your service, as it won't be running on
the right hosts. The right approach is to create a separate Ansible
role (called, say, *myrole-frontend*), and configure your playbook to
run it on hosts in the *frontend* Ansible group, e.g.:

```yaml
# Normal playbook for myrole
- hosts: myrole
  roles:
    - myrole
# Front-end customization for myrole
- hosts: frontend
  roles:
    - myrole-frontend
```

See for example the *noblogs* and *noblogs-frontend* roles in this
repository.

## My service needs to modify a configuration file created by another role

You might be tempted to use in-place edit commands like the
*blockinfile* Ansible module or similar others. **Don't**:
it is *extremely unsafe* in many scenarios.

You'll want to modify the original (parent) role instead, the
one that creates the configuration file you want to modify, and
parameterize the options you want to add. Set those options then
in your own configuration.

Let's consider an example: adding a logo to the *sso-server*.
This requires adding the *site_logo* parameter to the configuration
file /etc/sso/server.yml, which is created by the *sso-server* role
in float using the Ansible *template* module.

This means that every time Ansible runs the sso-server role, the file
will be overwritten with the templated contents (and the sso-server
will be restarted). If we were to modify it later in a different role,
there would be a window of time where the configuration would be
wrong (or incomplete). Furthermore, the final correctness of the
configuration would depend on the exact execution in sequence of
both the *sso-server* and your own *customized-sso-server* role.

Instead, a good principle to follow is that **every configuration
file must be modified by a single Ansible role**.

So, back to our sso-server example, a reasonable approach might
be to modify the float/sso-server role by adding a configurable
parameter in the server template:

```
{% if sso_site_logo is defined %}
site_logo: {{ sso_site_logo }}
{% endif %}
```

we then create a *customized-sso-server* Ansible role, that will
simply copy the logo into place somewhere, and create a playbook
that runs it on the sso-server hosts:

```
- hosts: sso-server
  roles: [customized-sso-server]
```

We can then define *sso_site_logo* in the configuration of our
own environment, and point it at the location where the files
are copied.


