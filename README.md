ai3/config
===

This repository contains the Ansible configuration for the
autistici.org services. The service scheduling itself is driven by
[ai3/float](https://git.autistici.org/ai3/float), an Ansible
orchestration plugin.

This README provides a high-level overview of the various services and
how they map to Ansible roles, as well as generic principles used when
creating this architecture. Further, more specific information can be
found in README files in the various subdirectories of
[roles/](roles/).

# Overview

## System-level Services

This repository contains a number of Ansible roles that extend and
customize the set of system-level services provided by *float*. We
call *system* services those that run on every host to provide
infrastructure-level functionality to higher level services, such as
authentication, monitoring, etc.

The roles customizing base *float* services are those with an *ai3-*
prefix ([ai3-base](roles/ai3-base/),
[ai3-prometheus](roles/ai3-prometheus/), etc).

Customization of the base *float* roles is achieved by adding matching
rules in our playbooks that run our customization roles on the
original role's group, e.g.:

```yaml
- hosts: prometheus
  roles:
    - ai3-prometheus
```

This approach unfortunately requires duplication of handlers in order
to reload the services (this is a common problem throughout this
repository).

The same playbook-based mechanism is used for service-specific
extensions to the base *float* roles. Consider for instance the
*noblogs* service, which requires additions to the NGINX configuration
(the *frontend* float service) and to the ACME certificate management
service: the additional Ansible roles *noblogs-frontend* and
*noblogs-acme* extend the base *frontend* and *acme* roles via the
playbook, resulting in the following playbook for the full noblogs
service:

```yaml
- hosts: frontend
  roles:
    - noblogs-frontend
- hosts: acme
  roles:
    - noblogs-acme
- hosts: noblogs
  roles:
    - noblogs
```

Note that in some occasional cases, the roles are misnamed for
historical reasons and do not follow this *service*-*base_service*
pattern (*ldap* and *auth-server-ldap*).

## Automation scripts

There are a large number of automation cron jobs in the A/I
architecture, which manage account-related configurations for a number
of services. These are grouped in the various *account-automation*
Ansible roles and have a structure which can be hard to navigate, so
we describe it here in detail:

* service roles depend on (or include) a specific
  *account-automation-foo* role
* all *account-automation-foo* depend on the
  *account-automation-common* role, which deals with shared setup such
  as installing the Debian packages for automation scripts, and
  generating shared configuration
* the various *account-automation-foo* roles primarily consist of a
  metadata spec (meta/main.yml) that sets up the associated cron jobs
  by invoking the *account-automation-cron* role with the job
  parameters

## High-level services

For testing purposes, services are split into major "groups" (mail,
web, noblogs, etc) whose specifications you can find in the various
services.\*.yml files.

The services are slowly being documented:

* [LDAP](roles/ldap/README.md)
* [mail services](roles/mail/README.md)
* [user-facing account management app](roles/accounts/README.md)
* [admin-facing account management app](roles/accountadmin/README.md)
* [jabber](roles/jabber/README.md)

# Enviroment per i test

Da questo repository, i commit di e su nuove branch ed anche sulla
master, innescano dei processi di test. Quando si chiede il merge di
una pull request dall'interfaccia web di gitlab, si vede l'avanzamento
della pipeline e la creazione dell'ambiente di test a cui è possibile
accedere per vedere le modifiche applicate. Il bottone View App, ha un
link simile a questo:
https://vm.investici.org/dash/i65WyixQslIyNNAzNLfQMzQ00zM0UNJRykvMTQUKZ-QXlxgCuUWJuUpWxgbmRjpKyQWlQKaOUmZuYjpISVJpTk5xamWqUq0OFrMMUc0yosQsI1SzjIk1KxYA
Dove abbiamo i comandi rapidi per accedere alle macchine virtuali
create da Vagrant.

Ogni ambiente VM di test utilizza una sua chiave SSH privata di root
temporanea, quindi bisogna anzitutto scaricarla: dalla pagina del job
"deploy", si segue il link "Artifacts" sulla destra, poi "Browse", per
poi scaricare il file *build-NNNN/ssh/vmkey*.

Per connettersi:

    ssh -i vmkey -J miscredenza.investici.org vagrant@10.178.116.10

E il comando da eseguire in locale dentro al repositori di Testbed,
per vedere il servizio attraverso un web browser.

    ./test-browser --proxy=localhost:9051 https://noblogs.org

Le macchine di ambiente di test rimangono attive per 3 ore, ma è anche
possibile fermarle a mano, con un bottone stop, nella pagina della
merge request.
