Troubleshooting
===

Ok hai configurato un ambiente di test e qualcosa non funziona. E ora?
Questo documento raccoglie suggerimenti vari su come capire cosa non
va, in ordine sparso.

# Errori comuni

## VM unreachable subito dopo "vagrant up"

Molto spesso capita che immediatamente dopo aver lanciato *vagrant up*
per avviare le VM, queste non siano raggiungibili con SSH. In tal caso
Ansible muore con errori tipo:

```
fatal: [host1]: UNREACHABLE! => {"changed": false, "msg": "Connection timed out.", "unreachable": true}
```

E' sufficiente riprovare.

# Procedure utili

## Aumentare la verbosità di Ansible

Quando c'è un errore conviene ri-eseguire *float* con l'opzione `-v`,
questo passerà l'opzione ad *ansible-playbook* risultando in una
grande quantità di output dettagliato sull'esecuzione di ciascun
task. Per esempio:

```shell
/path/to/float run -v site.yml
```

Spesso questo è sufficiente per capire cosa non va, perché l'output di
debugging contiene i messaggi di errore remoti.

## Diminuire la verbosità di Ansible

Ogni run di Ansible mostrerà l'esito di ogni task, anche quelli che hanno avuto
successo. Per mostrare solo i task "non ok":

```shell
/path/to/float run --stdout actionable site.yml
```

## Log di Ansible

Al termine di una run Ansible viene mostrato un riassunto
dell'esecuzione che mostra anche il numero di task falliti, ad
esempio:

```
PLAY RECAP ****************************************************************
host1                      : ok=175  changed=119  unreachable=0    failed=0
host2                      : ok=90   changed=64   unreachable=0    failed=1
```

Ansible interrompe l'esecuzione del playbook **per un singolo host**
se un task fallisce, ma continua l'esecuzione sugli altri host,
pertanto le ultime righe di log di `ansible-playbook` non
corrispondono necessariamente al task fallito.

Per individuare quale sia il task fallito bisogna leggere
`ansible.log` e cercare le righe che menzionano `FAILED` e l'hostname
della VM su cui e' fallito il task (in questo caso `host2`).

Ad esempio:

```
$ grep FAILED ansible.log
2017-11-12 20:53:57,072 p=82853 u=sand |  fatal: [host2]: FAILED! => {"changed": false, "failed": true, "msg": "Unable to start service docker-accounts_webapp.service: Job for docker-accounts_webapp.service failed because the control process exited with error code.\nSee \"systemctl status docker-accounts_webapp.service\" and \"journalctl -xe\" for details.\n"}
2017-11-12 20:53:57,531 p=82853 u=sand |  fatal: [host1]: FAILED! => {"changed": false, "failed": true, "msg": "Could not find the requested service docker-accounts_webapp.service: host"}
2017-11-12 20:53:57,922 p=82853 u=sand |  fatal: [host1]: FAILED! => {"changed": false, "failed": true, "msg": "Could not find the requested service docker-accounts_redis.service: host"}
```


