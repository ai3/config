Linee guida per costruire container
===

Volendo impacchettare un servizio dentro uno o più containers, ci sono
alcuni principi da seguire in modo da far funzionare al meglio le cose
con *float*. Float può più o meno coprire tutta la casistica
necessaria, ma è meglio standardizzare alcune metodologie e scelte
così che tutti i nostri container funzionino all'incirca allo stesso
modo.

## Suddivisione di un servizio in container

La visione più diffusa oggi per la strutturazione di un servizio in
uno o più container segue il modello "un container == un
processo". Noi invece la vediamo diversamente: per noi è meglio se un
container rappresenta un *servizio*, con una precisa e completa
interfaccia da e per il resto del mondo. Non ci vediamo niente di
male, entro limiti ragionevoli, ad avere più di un processo (più di un
demone, diciamo) in un singolo container se questi sono parte
integrante dello stesso servizio, e se sono fatti per essere
co-locati.

Alcuni esempi pratici possono chiarire meglio la questione:

* ci sembra superfluo usare due container diversi per un demone e per
  il suo exporter Prometheus - integrarli nello stesso container
  consente tra le altre cose di usare meccanismi "privati" come socket
  UNIX per far comunicare i due processi.

* alcuni servizi come *lurker* e *helpdesk* comprendono dei server
  SMTP che sono parte integrante della API del servizio, e son fatti
  per parlare con il processo principale esclusivamente in locale:
  anche qui ha senso eseguire il processo *smtpd* dentro lo stesso
  container.

* in altri casi i servizi hanno componenti che seguono chiare linee di
  demarcazione interne, e che dunque ha senso suddividere in più
  container, per esempio *pannello* e *memcache*, *irc* ed i suoi
  services, *zipkin* ha un container per l'applicazione web ed uno per
  il batch processing, etc...

Avendo più processi dentro uno stesso container è necessario un demone
*init* dentro il container. Però si pone un problema di gestione del
*lifecycle*: cosa fare quando un processo muore, magari per un errore?
Qui bisogna fare delle considerazioni che sono legate alla specifica
implementazione di *float*.

Per via del fatto che la "superficie di controllo" dell'automazione è
al livello della unit systemd (e che lì per esempio si trova il
monitoraggio degli errori, ed altro), a noi conviene che gli errori
fatali dei servizi emergano a tale livello: quindi il container deve
terminare quando c'è un problema fatale con i processi.

Esiste poi una seconda categoria di processi che ci fa comodo
trattare in maniera più rilassata: per esempio un errore in un
exporter Prometheus non è particolarmente interessante, il processo
può essere riavviato dentro il container stesso, senza disturbare il
processo principale.

Un ultimo requisito è la possibilità di lanciare uno script prima
dell'avvio dei processi principali: così infatti normalmente gestiamo
cose come aggiornamenti dei database etc.

Per ora la lista di demoni di init con le features descritte è
sfortunatamente breve:

* [https://chaperone.readthedocs.io/](Chaperone), la soluzione
  attualmente in uso. Funziona bene ma ha lo svantaggio di essere
  scritto in Python 3, che vuol dire che ci tiriamo dentro tutto
  l'environment Python 3 in ogni immagine e sono più di 100Mb.
  Purtroppo nel 2020 questo progetto pare non essere più sviluppato.

* [https://github.com/just-containers/s6-overlay](s6-overlay) pare
  essere una possibilità interessante basata su s6, un init system
  minimale. Abbiamo trovato un modo di far funzionare s6-overlay
  nell'ambiente container ristretto offerto da *float*
  ([ai3/docker/s6-base](https://git.autistici.org/ai3/docker/s6-base)),
  e siamo nel processo di sostituire chaperone con questa soluzione.

## Gerarchia di immagini

Per ridurre le risorse necessarie a distribuire e far funzionare
questi container, conviene organizzare i servizi in una gerarchia di
immagini via via sempre più specializzate, a seconda della loro
generalità. Per esempio, un servizio che richiede Apache potrà avere
la seguente gerarchia di immagini:

```
      (debian)
         |
         v
 ai3/docker/s6-base
         |
         v
 ai3/docker/apache2-base
         |
         v
 mio-servizio
```

In particolare, attualmente abbiamo a disposizione le seguenti
immagini, tutte basate su una versione minimale di Debian stretch
(*bitnami/minideb:stretch*):

* [ai3/docker/s6-base](https://git.autistici.org/ai3/docker/s6-base),
  Debian base più un *supervisor* minimale (*s6-overlay*), che serve
  per far girare più di un processo nel container.

* [ai3/docker/apache2-base](https://git.autistici.org/ai3/docker/apache2-base),
  un'immagine basata sulla precedente, che offre Apache2,
  configurabile a piacere dalle immagini "figlie". Esempio:
  [ai3/docker/roundcube](https://git.autistici.org/ai3/docker/roundcube).

* [ai3/docker/gunicorn-base](https://git.autistici.org/ai3/docker/gunicorn-base),
  un'immagine con *gunicorn*, per eseguire applicazioni web scritte in
  Python. Esempio: [pannello](https://git.autistici.org/ai3/pannello).

Questi repository hanno dei README con ulteriori dettagli su come
utilizzare le immagini.

## Configurazione

Per molti servizi, la configurazione necessaria può essere banalmente
passata nell'environment e dunque specificata in *services.yml*: in
questo caso basta configurare il servizio nell'immagine in modo da
usare queste variabili di environment.

Alcuni servizi hanno però bisogno di una configurazione più complessa
di quel che si può specificare nell'environment: in questi casi la
configurazione verrà creata in Ansible, in una directory dedicata
sotto */etc*, la quale sarà poi montata nel container a runtime
(aggiungendo la directory ai mountpoint in *services.yml*).

Spesso conviene modificare leggermente l'applicazione (o usare uno
script di startup per prepararla) di modo da poter usare un'unica
directory per tutta la configurazione necessaria, così da semplificare
l'interazione tra Ansible ed il container. Un esempio di questa
strategia per un'applicazione PHP, con un layout di configurazione
complesso, è in
[docker-roundcube](https://git.autistici.org/ai3/docker/roundcube):
qui piazziamo dei link simbolici dentro l'immagine ad una directory
*/etc/roundcube* che nell'immagine non esiste, ma verrà montata a
runtime.

## Utenti e permessi

Normalmente *float* crea un utente ed un gruppo dedicati per ciascun
servizio, ed esegue i container con quell'utente. Questo implica che
non è possibile sapere quale sia questo utente al momento della
costruzione dell'immagine Docker: è a tutti gli effetti un parametro
applicato a *runtime*. Bisogna quindi avere cura di costruire immagini
Docker che possano essere eseguite da un utente qualsiasi. All'atto
pratico questo significa:

* configurare il servizio in modo da non fare setuid()
* non bindare una porta privilegiata (<1024)
* se il servizio deve scrivere dati su disco:
  * configurare il servizio per usare */tmp* oppure */run/lock*, a
    seconda, che sono automaticamente montate da *float* come tmpfs
    con i permessi giusti
  * per dati che devono persistere attraverso i restart del container,
    bisogna creare (in Ansible!) una directory con i permessi giusti,
    e montarla nel container. Dal punto di vista del container, ci si
    può aspettare di avere una directory standard (e.g. */data*)
    disponibile per la scrittura. Un esempio di come gestire questa
    situazione (con dei link) è in
    [docker-roundcube](https://git.autistici.org/ai3/docker/roundcube).
* i dati degli utenti relativi al servizio dovranno essere leggibili
  (e scrivibili, a seconda dell'applicazione) dall'utente dedicato, e
  montati anch'essi in una directory opportuna (per es. */home/mail*).

## Porte

Normalmente le immagini Docker esportano la porta standard del
servizio che implementano, che poi vengono rimappate a runtime a
seconda delle esigenze. Noi, siccome ancora non siamo sicuri di come
gestire questa parte (l'interfaccia alla gestione di rete non è
stabilizzata, diciamo), adottiamo un approccio leggermente diverso: ci
aspettiamo che il container bindi ad una porta che possiamo
specificare a runtime attraverso una variabile di environment, così
che per Docker il mapping delle porte sia sempre 1:1.

Per fare ciò, l'immagine non deve hard-codare una specifica porta per
il servizio, ma usare una variabile di environment (a scelta), e non
dichiarare nessuna direttiva `EXPOSE` nel Dockerfile.

Un paio di esempi differenti:

* l'immagine
  [docker-apache2-base](https://git.autistici.org/ai3/docker/apache2-base)
  e tutti i suoi derivati usano la variabile `APACHE_PORT` per
  selezionare la porta da usare (apache2 cortesemente può utilizzare
  variabili di environment direttamente nei suoi file di
  configurazione). In
  [env/test-full/services.yml](env/test-full/services.yml) si possono
  vedere alcuni esempi di come poi il servizio viene configurato.
  L'accortezza da notare è che bisogna usare `${APACHE_PORT}` nelle
  direttive `<VirtualHost>` dei siti configurati:

```
<VirtualHost *:${APACHE_PORT}>
    ...
</VirtualHost>
```

* l'immagine [pannello](https://git.autistici.org/ai3/pannello), che
  usa docker-gunicorn-base, legge la variabile di environment
  `BIND_ADDR` direttamente dal file di configurazione
  [gunicorn.conf](https://git.autistici.org/ai3/pannello/blob/master/docker/gunicorn.conf).
