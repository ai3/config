# Makefile to regenerate all the graphviz renderings in
# the role-specific directories.

DOTS = $(wildcard docs/*.dot) $(wildcard roles/*/*.dot)
SVGS = $(DOTS:%.dot=%.svg)
PNGS = $(DOTS:%.dot=%.png)

all: $(SVGS) $(PNGS)

%.svg: %.dot
	dot -Tsvg -o$@ $<

%.png: %.dot
	dot -Tpng -o$@ $<

